#include "onewire.h"

#define LED_PIN PICO_DEFAULT_LED_PIN

// Function prototypes
bool onewire_reset();
void onewire_write_bit(bool bit);
bool onewire_read_bit();
void onewire_write_byte(uint8_t byte);
uint8_t onewire_read_byte();

int main()
{
    uint8_t code[8] = {0x00};

    stdio_init_all();
    setup_default_uart();

    gpio_init(LED_PIN);
    gpio_init(ONEWIRE_PIN);

    gpio_set_dir(LED_PIN, GPIO_OUT);
    gpio_set_dir(ONEWIRE_PIN, GPIO_OUT);

    printf("Raspberry Pi Pico - works. Curr time: %.2f [s]\n", time_us_32() / 1000000);
    gpio_put(LED_PIN, 1);

    while (1)
    {
        get_code(&code);
        print_code(&code);

        gpio_put(LED_PIN, 1);
        sleep_ms(500);
        gpio_put(LED_PIN, 0);
        sleep_ms(500);
    }

    return 0;
}
