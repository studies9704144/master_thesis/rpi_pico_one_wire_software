#include "onewire.h"

bool onewire_reset()
{
    // Drive the line low and then release it

    gpio_set_dir(ONEWIRE_PIN, GPIO_OUT);
    gpio_put(ONEWIRE_PIN, 0);
    sleep_us(480); // Min 480us reset pulse

    // Release line and wait for presence
    gpio_set_dir(ONEWIRE_PIN, GPIO_IN);
    sleep_us(70); // Wait for presence pulse

    bool presence = !gpio_get(ONEWIRE_PIN);
    sleep_us(410); // Complete the waiting period

    return presence;
}

void onewire_write_bit(bool bit)
{
    gpio_set_dir(ONEWIRE_PIN, GPIO_OUT);
    gpio_put(ONEWIRE_PIN, false);
    sleep_us(3); // (spec 1-15us)
    if (bit == true)
    {
        gpio_put(ONEWIRE_PIN, true);
        sleep_us(55);
    }
    else
    {
        sleep_us(60); // (spec 60-120us)
        gpio_put(ONEWIRE_PIN, true);
        sleep_us(5); // allow bus to float high before next bit_out
    }
}

bool onewire_read_bit()
{
    bool bit = false;
    gpio_set_dir(ONEWIRE_PIN, GPIO_OUT);
    gpio_put(ONEWIRE_PIN, false);
    sleep_us(3); // (spec 1-15us)
    gpio_set_dir(ONEWIRE_PIN, GPIO_IN);
    sleep_us(3); // (spec read within 15us)
    bit = gpio_get(ONEWIRE_PIN);
    sleep_us(45);
    return bit;
}

void onewire_write_byte(uint8_t byte)
{
    for (int i = 0; i < 8; i++)
    {
        onewire_write_bit((bool)(byte & 0x01));
        byte >>= 1;
    }
}

uint8_t onewire_read_byte()
{
    uint8_t byte = 0x00;
    for (int i = 0; i < 8; i++)
    {
        if (onewire_read_bit())
        {
            byte |= (1 << i);
        }
    }
    return byte;
}

void get_code(uint8_t *a_code)
{
    if (onewire_reset())
    {
        onewire_write_byte(0x33); // Skip ROM
        for (int i = 0; i < 8; i++)
        {
            a_code[i] = onewire_read_byte();
        }
    }
    else
    {
        printf("No device present\n");
    }
}

void print_code(uint8_t *code)
{
    printf("Code:");
    for (int i = 0; i < 8; i++)
    {
        printf(" %02X", code[i]);
    }
    printf("\r\n");
}