#ifndef ONEWIRE_H
#define ONEWIRE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "pico/stdlib.h"
#include "hardware/gpio.h"
#include "hardware/timer.h"

#define ONEWIRE_PIN 15

bool onewire_reset();

void onewire_write_bit(bool bit);

bool onewire_read_bit();

void onewire_write_byte(uint8_t byte);

uint8_t onewire_read_byte();

void get_code(uint8_t *code);

void print_code(uint8_t *code);

#endif // ONEWIRE_H